
ARG POSTGRES_VERSION=13.2-alpine
ARG TZ=UTC

FROM "postgres:${POSTGRES_VERSION}" AS development-env

LABEL authors="Ricardo-Melo-Martins"
LABEL description="RMM Docker Database PostgreSQL"

#####################################
# Scripts
#####################################

COPY ./script/sakila/sakila-schema.sql /docker-entrypoint-initdb.d/step_01.sql
COPY ./script/sakila/sakila-data.sql /docker-entrypoint-initdb.d/step_02.sql

# localedef -i pt_BR -f UTF-8 pt_BR.UTF-8
# RUN localedef -i pt_BR -c -f UTF-8 -A /usr/share/locale/locale.alias pt_BR.UTF-8
# ENV LANG pt_BR.utf8  ou  localectl set-locale LANG=pt_BR.UTF-8
ENV LANG=pt_BR.UTF-8


#####################################
# Timezone
#####################################

ENV TZ ${TZ}
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

VOLUME /var/lib/postgresql/data

EXPOSE 5432




