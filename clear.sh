#/bin/bash

# Ricardo Melo Martins
# 
echo "docker clear"

docker container ls -a

docker container stop db_postgres13

docker container rm db_postgres13 -v

docker image ls
docker image rm rmm_postgres13

docker network ls
docker network rm rmm-databases

docker container ls -aq